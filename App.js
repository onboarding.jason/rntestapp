/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';

import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import ContactScreen from './components/ContactScreen';
import EditScreen from './components/EditScreen';

function App() {
  const Stack = createNativeStackNavigator();

  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="ContactScreen"
          component={ContactScreen}
          options={() => ({title: 'Contacts'})}
        />
        <Stack.Screen
          name="EditScreen"
          component={EditScreen}
          options={() => ({title: ''})}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
