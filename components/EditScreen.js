import React, {useLayoutEffect, useRef, useState} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Alert,
} from 'react-native';

function EditScreen({navigation, route}) {
  const {params} = route;

  const lastNameRef = useRef();
  const emailRef = useRef();
  const phoneRef = useRef();

  const [firstName, setFirstName] = useState(params?.contactDetail?.firstName);
  const [lastName, setLastName] = useState(params?.contactDetail?.lastName);
  const [email, setEmail] = useState(params?.contactDetail?.email);
  const [phone, setPhone] = useState(params?.contactDetail?.phone);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <TouchableOpacity onPress={() => navigation.navigate('ContactScreen')}>
          <Text style={styles.headerSideText}>Cancel</Text>
        </TouchableOpacity>
      ),
      headerRight: () => (
        <TouchableOpacity
          onPress={() => {
            if (firstName && lastName) {
              navigation.navigate('ContactScreen', {
                id:
                  params?.contactDetail?.id || Math.floor(Math.random() * 1000),
                firstName,
                lastName,
                email,
                phone,
              });
            } else {
              Alert.alert(
                'Missing Required Fields',
                'First Name & Last Name is Required!',
              );
            }
          }}>
          <Text style={styles.headerSideText}>Save</Text>
        </TouchableOpacity>
      ),
    });
  }, [navigation, firstName, lastName, email, phone]);

  return (
    <View style={styles.mainWrapper}>
      <View style={styles.circleWrapper}>
        <View style={styles.circle} />
      </View>
      <Text style={styles.headerText}>Main Information</Text>
      <View style={styles.fieldWrapper}>
        <Text style={styles.fieldTitle}>First Name</Text>
        <TextInput
          style={styles.fieldText}
          value={firstName}
          onChangeText={setFirstName}
          returnKeyType={'next'}
          onSubmitEditing={() => lastNameRef.current.focus()}
        />
      </View>
      <View style={[styles.fieldWrapper, {borderBottomWidth: 0}]}>
        <Text style={styles.fieldTitle}>Last Name</Text>
        <TextInput
          ref={lastNameRef}
          style={styles.fieldText}
          value={lastName}
          onChangeText={setLastName}
          returnKeyType={'next'}
          onSubmitEditing={() => emailRef.current.focus()}
        />
      </View>
      <Text style={styles.headerText}>Sub Information</Text>
      <View style={styles.fieldWrapper}>
        <Text style={styles.fieldTitle}>Email</Text>
        <TextInput
          ref={emailRef}
          style={styles.fieldText}
          value={email}
          onChangeText={setEmail}
          returnKeyType={'next'}
          onSubmitEditing={() => phoneRef.current.focus()}
        />
      </View>
      <View style={styles.fieldWrapper}>
        <Text style={styles.fieldTitle}>Phone</Text>
        <TextInput
          ref={phoneRef}
          style={styles.fieldText}
          value={phone}
          onChangeText={setPhone}
        />
      </View>
    </View>
  );
}

export default EditScreen;

const styles = StyleSheet.create({
  mainWrapper: {
    backgroundColor: 'white',
    flex: 1,
  },
  fieldWrapper: {
    flexDirection: 'row',
    borderBottomWidth: 0.2,
    borderBottomColor: 'grey',
    margin: 5,
    paddingBottom: 5,
    alignItems: 'center',
  },
  fieldTitle: {
    flexDirection: 'column',
    flex: 0.3,
    fontSize: 15,
  },
  fieldText: {
    flexDirection: 'column',
    flex: 0.7,
    fontSize: 15,
    borderWidth: 0.2,
    borderRadius: 3,
    padding: 5,
  },
  headerText: {
    backgroundColor: '#f9f9f9',
    fontWeight: 'bold',
    fontSize: 18,
    padding: 5,
  },
  circleWrapper: {
    alignItems: 'center',
    marginVertical: 20,
  },
  circle: {
    width: 100,
    height: 100,
    borderRadius: 50,
    backgroundColor: '#ff8c00',
  },
  headerSideText: {
    color: '#ff8c00',
    fontSize: 18,
  },
});
