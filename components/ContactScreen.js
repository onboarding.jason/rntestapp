import React, {useEffect, useLayoutEffect, useState} from 'react';
import {
  FlatList,
  TouchableOpacity,
  View,
  Text,
  TextInput,
  StyleSheet,
} from 'react-native';
import * as rawData from '../assets/data.json';
import ContactListRow from './ContactListRow';

function ContactScreen({navigation, route}) {
  const [data, setData] = useState([]);
  const [isRefreshing, setRefreshing] = useState(false);
  const [isSearch, setSearch] = useState(false);
  const [searchTerm, setSearchTerm] = useState('');
  const [filteredData, setFilteredData] = useState([]);

  function resetData() {
    setRefreshing(true);
    setData(rawData.default);
    setRefreshing(false);
  }

  useEffect(() => {
    resetData();
  }, []);

  useEffect(() => {
    if (route.params?.id) {
      const dataIndex = data.findIndex(d => d.id === route.params?.id);

      if (dataIndex > -1) {
        let newData = [...data];
        newData[dataIndex] = route.params;
        setData(newData);
      } else {
        setData(prevData => [...prevData, route.params]);
      }
    }
  }, [route]);

  useEffect(() => {
    if (searchTerm && searchTerm !== '') {
      setFilteredData(
        data.filter(
          d =>
            d.firstName.includes(searchTerm) || d.lastName.includes(searchTerm),
        ),
      );
    } else {
      setFilteredData(data);
    }
  }, [searchTerm, data]);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <TouchableOpacity onPress={() => setSearch(!isSearch)}>
          <Text style={styles.headerLeftText}>Search</Text>
        </TouchableOpacity>
      ),
      headerRight: () => (
        <TouchableOpacity onPress={() => navigation.navigate('EditScreen')}>
          <Text style={styles.headerRightText}>+</Text>
        </TouchableOpacity>
      ),
    });
  });

  return (
    <FlatList
      style={{backgroundColor: 'white'}}
      data={filteredData}
      renderItem={({item}) => (
        <ContactListRow item={item} navigation={navigation} />
      )}
      onRefresh={() => resetData()}
      refreshing={isRefreshing}
      ListHeaderComponent={() =>
        isSearch && (
          <View style={styles.searchBarWrapper}>
            <TextInput
              style={styles.searchBar}
              value={searchTerm}
              onChangeText={setSearchTerm}
            />
          </View>
        )
      }
    />
  );
}

export default ContactScreen;

const styles = StyleSheet.create({
  headerLeftText: {
    color: '#ff8c00',
    fontWeight: 'bold',
  },
  headerRightText: {
    color: '#ff8c00',
    fontSize: 30,
  },
  searchBarWrapper: {
    backgroundColor: '#ff8c00',
    padding: 10,
  },
  searchBar: {
    backgroundColor: 'white',
    padding: 10,
    borderWidth: 0.5,
    borderRadius: 5,
  },
});
