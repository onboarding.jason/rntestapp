import React from 'react';
import {TouchableOpacity, View, Text, StyleSheet} from 'react-native';

const ContactListRow = ({item, navigation}) => {
  return (
    <TouchableOpacity
      onPress={() => navigation.navigate('EditScreen', {contactDetail: item})}>
      <View style={styles.listRow}>
        <View style={styles.circle} />
        <Text style={styles.name}>
          {item.firstName} {item.lastName}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export default ContactListRow;

const styles = StyleSheet.create({
  circle: {
    width: 50,
    height: 50,
    borderRadius: 50,
    backgroundColor: '#ff8c00',
  },
  listRow: {
    padding: 10,
    borderBottomWidth: 0.5,
    borderBottomColor: 'grey',
    flexDirection: 'row',
  },
  name: {
    alignSelf: 'center',
    paddingHorizontal: 10,
    fontSize: 15,
  },
});
